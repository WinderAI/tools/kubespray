# Kubeflow

## 1. Prerequisites

1. Connect to your kubernetes cluster

## 2. Install

This will fail the first time because required CRDs are not present. The while look spins until it works.

```
while ! kubectl apply -k "github.com/kubeflow/manifests/example?ref=v1.3.0"; do echo "Retrying to apply resources"; sleep 10; done
```

## 3. Fix

```
# For https://github.com/kubeflow/manifests/pull/1883
kubectl -n auth set env deployment/dex KUBERNETES_POD_NAMESPACE=auth
# For https://github.com/kubeflow/manifests/tree/master#nodeport--loadbalancer--ingress
# TODO: This is not secure. Only for local.
kubectl -n kubeflow set env deployment/centraldashboard APP_SECURE_COOKIES=false
kubectl -n kubeflow set env deployment/jupyter-web-app-deployment APP_SECURE_COOKIES=false
kubectl -n kubeflow set env deployment/tensorboards-web-app-deployment APP_SECURE_COOKIES=false
```
## 4. Port Forward

```
kubectl port-forward $(kubectl get pods -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].metadata.name}') -n istio-system 8080:8080
```

Once you see the login screen, the username is `user@example.com` and the password is `12341234`.
