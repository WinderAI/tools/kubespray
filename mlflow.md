# mlflow

## 1. Prerequisites

1. Connect to your kubernetes cluster

## 2. Install

```
terraform init
KUBE_CONFIG_PATH=~/source/kubernetes-sigs/kubespray/kubespray-do.conf terraform apply -auto-approve
```

## 3. Fix

```
```

## 4. Port Forward

```
kubectl port-forward svc/combinator-mlflow -n mlflow 5000:5000
```

Once you see the login screen, the username is `user@example.com` and the password is `12341234`.
