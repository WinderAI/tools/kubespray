# kubespray

Winder Research specific Kubespray installation for bare-metal k8s.

## Instructions

**[Official documentation](https://github.com/kubernetes-sigs/kubespray)**

### 1. Clone the Repository and Install Prerequisites

```
mkdir -p ~/source/kubernetes-sigs
cd ~/source/kubernetes-sigs
git clone https://github.com/kubernetes-sigs/kubespray.git
cd kubespray
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

:warning: If you're running on an M1 Mac, then you'll need to battle through installing the cryptography package. For me, altering the requirements to the newest version worked. :warning:

Export some variables for later use:

```
export SSH_USERNAME=your_ssh_user
export SSH_MASTER_IP=1.2.3.4
```

### 2. Configure Your Cluster

```
cp -rfp inventory/sample inventory/mycluster
declare -a IPS=(10.132.0.4) # Replace with the private IP addresses of your nodes (find out via `ifconfig`)
CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}
```

:question: If you're ssh-ing into this machine via another IP address, then you'll need to edit the `ansible_host` setting in `inventory/mycluster/hosts.yaml` and replace it with the public IP address from $SSH_MASTER_IP. :question:

Make sure you can access the cluster (this will hang if no access):

```
ansible -i inventory/mycluster/hosts.yaml -m ping all
```

Review the default settings

```
cat inventory/mycluster/group_vars/all/all.yml
cat inventory/mycluster/group_vars/k8s_cluster/k8s-cluster.yml
```

:question: For GPU enabled machines, pay close attention to the `nvidia_accelerator_enabled` section in `inventory/mycluster/group_vars/k8s_cluster/k8s-cluster.yml` :question:

### 3. Create the Cluster

```
ansible-playbook -i inventory/mycluster/hosts.yaml  --become --become-user=root cluster.yml --flush-cache
```

### 4. Obtain the Kubeconfig

:warning: This is the administrative kubeconfig. :warning:

```
ssh -i ~/.ssh/id_rsa $SSH_USERNAME@$SSH_MASTER_IP 'sudo chown -R $(whoami):$(whoami) /etc/kubernetes/admin.conf'
scp -i ~/.ssh/id_rsa $SSH_USERNAME@$SSH_MASTER_IP:/etc/kubernetes/admin.conf kubespray-do.conf
```

:question: If you want to access the k8s API externally, then you can edit the kubeconfig and change the `server` ip address to the remote IP. Make sure port 6443 is open. `nmap -p6443 -Pn $SSH_MASTER_IP` :question:

### 5. Tunnel and Test

In one terminal run:

```
ssh -L 6443:localhost:6443 -i ~/.ssh/id_rsa $SSH_USERNAME@$SSH_MASTER_IP
```

This is setting up a tunnel between your machine and the master, on port 6443, the k8s API port.

Now connect to the API:

```
export KUBECONFIG=$(pwd)/kubespray-do.conf
kubectl get node
```

It should respond with your nodes.

### 6. Configure

Add a volume implementation and make it the default pvc handler:
```
kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/v1.1.2/deploy/longhorn.yaml
kubectl patch storageclass longhorn -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

TODO: Make this part of kubespray?

# Anthos

Note that this only works on amd64 machines. I haven't found a way to build for arm64 yet.

## 1. Install Prerequisites

```
brew install google-cloud-sdk kubectl
gcloud auth login --update-adc
```

```
mkdir baremetal
cd baremetal
gsutil cp gs://anthos-baremetal-release/bmctl/1.8.0/linux-amd64/bmctl bmctl
chmod a+x bmctl
./bmctl -h
```